FROM ubuntu
RUN apt update \
    && apt install git gcc g++ make cmake pkgconf -y \
    && git clone https://github.com/google/benchmark.git \
    && cd benchmark \
    && git clone https://github.com/google/googletest.git \
    && cmake -E make_directory "build" \
    && cmake -E chdir "build" cmake -DBENCHMARK_DOWNLOAD_DEPENDENCIES=on -DCMAKE_BUILD_TYPE=Release ../ \
    && cmake --build "build" --config Release
RUN cd /usr/src \
    && git clone https://github.com/google/googletest.git \
    && cd / \
    && git clone https://github.com/abseil/abseil-cpp.git \
    && cd abseil-cpp \
    && mkdir build \
    && cd build \
    && cmake -DABSL_BUILD_TESTING=ON -DABSL_PROPAGATE_CXX_STD=ON -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_LIBS=ON -DABSL_USE_GOOGLETEST_HEAD=ON -DCMAKE_CXX_STANDARD=14 -DCMAKE_INSTALL_PREFIX=build .. \
    && echo $PWD && ls -la \
    && export PKG_CONFIG=/usr/lib/pkgconfig \
    && make -B -j install
RUN ls -la /abseil-cpp/build/build/lib/pkgconfig
RUN export PKG_CONFIG=/usr/lib/pkgconfig \
    && cd /abseil-cpp \
    && git clone https://github.com/google/re2.git \
    && mkdir re2/absl \
    && cp -r /abseil-cpp/absl/* re2/absl \
    && cd re2 \
    && mkdir /artifacts \
    && make -B install DESTDIR=/artifacts CXXFLAGS='-O3 -g -fPIC'
